﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;


namespace ManaTime
{
    class Ennemis : Personnages 
    {
        //FIELDS
        //Deplacement
        protected bool arret_enn = true;
        protected double angle = 0;

        int center_x;
        int center_y;

        int radius_x;
        int radius_y;
        double rot_speed;

        TimeSpan appear;




        //CONSTRUCTOR
        public Ennemis(string sprite, int x, int y, TimeSpan time, int radius_x, int radius_y, double rot_speed, int life) 
            : base(sprite, life)
        {
            perso_pos.X = x + radius_x;
            perso_pos.Y = y - radius_y;

            center_x = x;
            center_y = y;

            this.radius_x = radius_x;
            this.radius_y = radius_y; 
            this.rot_speed = rot_speed;

            appear = time;
        }
        


        
        // METHODS



        // Methodes de Deplacement
        // Mouvement elliptique
        public int Move_Ellip_Y()
        {
            return (int)(center_y + (radius_y * Math.Sin(angle += rot_speed)));
        }


        public int Move_Ellip_X()
        {
            return (int)(center_x + (radius_x * Math.Cos(angle += rot_speed)));
        }


        public bool Arret(int y)
        {
            return (perso_pos.Y <= y + y * 0.05 && perso_pos.Y >= y - y * 0.05); 

        }


        //UPDATE & DRAW
        public void Update(GameTime gameTime)
        {
            if (!arret_enn && appear <= gameTime.TotalGameTime)   
            {
                // Mouvement Elliptique
                perso_pos.X = Move_Ellip_X(); 
                perso_pos.Y = Move_Ellip_Y(); 

            }

            else 
                arret_enn = false;
        }


     
     }
}
