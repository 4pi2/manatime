﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;



namespace ManaTime
{
    class Joueur : Personnages
    {
        //FIELDS
        int moveSpeed = 4;

        protected bool estEnVie = true;

        bool keydown  = true;

        Tir1 tir;

        //CONSTRUCTOR
        public Joueur(string sprite, int life = 5)
            : base(sprite, life)
        {
            tir = new Tir1("hitbox_joueur", perso_pos.X + this.sprite.Bounds.Width/3, perso_pos.Y);
        }


        //METHODS
     
        
       // UPDATE & DRAW
        public void Update(KeyboardState state)
        {
            //Déplacement
            if (state.IsKeyDown(Keys.Up))
                perso_pos.Y -= moveSpeed;
            if (state.IsKeyDown(Keys.Down))
                perso_pos.Y += moveSpeed;
            if (state.IsKeyDown(Keys.Right))
                perso_pos.X += moveSpeed;
            if (state.IsKeyDown(Keys.Left))
                perso_pos.X -= moveSpeed;


            //Collisions avec les bords
            if (perso_pos.Y <= 0)
                perso_pos.Y = 0;
            if (perso_pos.Y + sprite.Bounds.Height >= Game1.Resolution.Y)
                perso_pos.Y = (int)(Game1.Resolution.Y) - sprite.Bounds.Height;

            if (perso_pos.X <= 0)
                perso_pos.X = 0;
            if (perso_pos.X + sprite.Bounds.Width >= Game1.bg_width)
                perso_pos.X = Game1.bg_width - sprite.Bounds.Width;

            //tir
            if (state.IsKeyDown(Keys.Space))
                keydown = false;
            if (!keydown)
                tir.Update(state);


            //collisions
        }
        


        public override void Draw(SpriteBatch sb)
        {
            base.Draw(sb);

           // if (Keyboard.GetState().IsKeyDown(Keys.Space))
                tir.Draw(sb);
        }
    }
}
