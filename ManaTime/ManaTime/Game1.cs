using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace ManaTime
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GameTime gametime = new GameTime();

        public new static ContentManager Content;
        public static int bg_width; 
        public static Vector2 Resolution;
 

        Level level_test;
        Joueur joueur;
        SeqEnn1 seqenn_test;
        Interface coeur_test;

		
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            base.Content.RootDirectory = "Content";
            Content = base.Content;
        }

        public void ChangeResolution(GraphicsDeviceManager g, int width = 800, int height = 600)
        {
            Game1.Resolution = new Vector2(width, height);
            g.PreferredBackBufferWidth = width;
            g.PreferredBackBufferHeight = height;
            g.ApplyChanges();
        }


        protected override void Initialize()
        {
            ChangeResolution(graphics);
            bg_width = (int)(Resolution.X * 0.7f);

            joueur = new Joueur("ragnarok2");
            level_test = new Level("test", "d�cor_lvl1-v2");
            seqenn_test = new SeqEnn1();
            coeur_test = new Interface("coeur", joueur);

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

       
        protected override void UnloadContent()
        {
            Content.Unload();
        }

       
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();

            level_test.Update();

            joueur.Update(Keyboard.GetState());

            seqenn_test.Update(gameTime);
             
            base.Update(gameTime);
        }

    
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkSlateBlue);

            spriteBatch.Begin();

            level_test.Draw(spriteBatch);
            joueur.Draw(spriteBatch);

           
            seqenn_test.Draw(spriteBatch, gameTime);
            coeur_test.Draw(spriteBatch);


            

            spriteBatch.End();



            base.Draw(gameTime);
        }
    }
}
