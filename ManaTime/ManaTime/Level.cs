﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ManaTime
{
    class Level
    {
        string name;
        Texture2D background;
        Rectangle background_pos, background_pos2;
        int bg_speed = 3;
        //Musique à ajouter 
        //Boss boss;
        //Structure de données ennemis;


        //Constructeur
        public Level(string name, string background/*la suite*/)
        {
            this.name = name;
            this.background = Game1.Content.Load<Texture2D>(background);
            background_pos = new Rectangle(0, 0, (int) (Game1.bg_width), (int) Game1.Resolution.Y);
            background_pos2 = new Rectangle(0, -(int)Game1.Resolution.Y, (int)(Game1.bg_width), (int)Game1.Resolution.Y);
        }



        public void Update()
        {
            background_pos.Y = (background_pos.Y > Game1.Resolution.Y) ? background_pos2.Y - background_pos2.Height + bg_speed: background_pos.Y + bg_speed; 
            background_pos2.Y = (background_pos2.Y > Game1.Resolution.Y) ? background_pos.Y - background_pos.Height + bg_speed : background_pos2.Y + bg_speed; 
        }

        public void Draw(SpriteBatch sb)
        {
            sb.Draw(background, background_pos, Color.White);
            sb.Draw(background, background_pos2, Color.White);
        }



    }
}
