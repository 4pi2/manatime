﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;


namespace ManaTime
{
    class Tir1
    {
        //FIELDS
        //deplacement
        private Texture2D tir;
        private Rectangle pos_tir;

        //constructeur
        public Tir1(string tir, int x, int y)
        {
            this.tir = Game1.Content.Load<Texture2D>(tir);
            pos_tir = new Rectangle(x, y, this.tir.Bounds.Width, this.tir.Bounds.Height);

        }


        //METHODS

        //UPDATE & DRAW
        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(tir, pos_tir, Color.White);
        }


        public void Update(KeyboardState state)
        {
            //Deplacement
             pos_tir.Y -= 5;
        }
    }
}