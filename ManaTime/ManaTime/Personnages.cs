﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ManaTime
{
    class Personnages
    {
        protected Texture2D sprite;
        //Pattern
        protected Rectangle perso_pos;

        private bool alive = true;
        public int life;

        //Constructeur 
        public Personnages(string sprite, int life)
        {
            this.sprite = Game1.Content.Load<Texture2D>(sprite);
            perso_pos = new Rectangle((int)(Game1.bg_width) / 2, (int)(Game1.Resolution.Y * 0.8f), this.sprite.Bounds.Width, this.sprite.Bounds.Height);

            this.life = life;
        }
        


        //METHODS



        //PROPERTIES
        public int sprite_width
        {
            get { return sprite.Bounds.Width; }
        }


        public int sprite_height
        {
            get { return sprite.Bounds.Height; }
        }


        public bool isAlive
        {
            get { return alive; }
            set { alive = value; }
        }


        public int XCoord
        {
            get { return perso_pos.X; }
        }

        public int YCoord
        {
            get { return perso_pos.Y; }
        }


        public Rectangle HitBox
        {
            get { return new Rectangle(perso_pos.X, perso_pos.Y, this.sprite.Bounds.Width, this.sprite.Bounds.Height); }
        }


        //UPDATE & DRAW 


        public virtual void Draw(SpriteBatch spritebatch)
        {
            if(isAlive)
                spritebatch.Draw(sprite, perso_pos, Color.White); 
        }
    }
}
