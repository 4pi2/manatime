﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;


namespace ManaTime
{
    class SeqEnn1
    {
        //FIELDS
        Stack<Ennemis> pile_enn = new Stack<Ennemis>();
        Stack<Ennemis> pile_enn2 = new Stack<Ennemis>();

        int dep = 2;

        //CONSTRUCTOR
        public SeqEnn1()
        {
            for (int i = 0; i <= 75; i++)
            {
                pile_enn.Push(new Ennemis("falcon_sniper", 0, -50,   new TimeSpan(0, 0, 0, 0, i * 100),  //Coords centre, temps d'apparition
                                            (int)(Game1.bg_width * 0.5 - 43), (int)(Game1.bg_width * 0.6), 0.01,    1));//Parametres de la trajectoire + Vie
                                            

                pile_enn2.Push(new Ennemis("falcon_sniper", Game1.bg_width, -50,     new TimeSpan(0, 0, 0, 0, i * 100),
                                           -(int)(Game1.bg_width * 0.5 + 43), (int)(Game1.bg_width * 0.6), 0.01,    1));
            }
        }



        //METHODS


        //UPDATE & DRAW
        public void Update(GameTime gameTime)
        {
            //Update de la 1ere pile
            foreach (Ennemis ennemis in pile_enn)
            {
                if (ennemis.XCoord < -ennemis.sprite_width)
                    ennemis.isAlive = false;

                if (gameTime.TotalGameTime >= new TimeSpan(0, 0, dep) && ennemis.isAlive)
                    ennemis.Update(gameTime);
            }



            //Update de la 2nde pile
            foreach (Ennemis ennemis in pile_enn2)
            {
                if (ennemis.XCoord > Game1.bg_width)
                    ennemis.isAlive = false;

                if (gameTime.TotalGameTime >= new TimeSpan(0, 0, dep) && ennemis.isAlive)
                    ennemis.Update(gameTime);
            }
        }





        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            //Draw de la 1ere pile
            foreach (Ennemis ennemis in pile_enn)
            {
                if (ennemis.XCoord < -ennemis.sprite_width)
                    ennemis.isAlive = false;

                if (gameTime.TotalGameTime >= new TimeSpan(0, 0, dep) && ennemis.isAlive)
                    ennemis.Draw(spriteBatch);

            }

        

            //Draw de la 2nde pile
            foreach (Ennemis ennemis in pile_enn2)
            { 
                if (ennemis.XCoord > Game1.bg_width)
                    ennemis.isAlive = false;

                if (gameTime.TotalGameTime >= new TimeSpan(0, 0, dep) && ennemis.isAlive)
                    ennemis.Draw(spriteBatch);

            }
        }
    }
}
