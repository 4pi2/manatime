﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace ManaTime
{
    class Interface
    {
        //fields
        Texture2D sprite;
        Texture2D fond;

        int marge = 40;


        Stack<Rectangle> vie = new Stack<Rectangle>();
        //int X = (int) (Game1.bg_with + marge);
        /*
           Modifier le système de vie ?
           Essayer de le caser dans Joueur ou ailleur ?
           Cette méthode est moche et de préférence provisoire
         */

        //Constructeurs
        public Interface(string sprite, Joueur joueur)
        {
            this.sprite = Game1.Content.Load<Texture2D>(sprite);
            fond = Game1.Content.Load<Texture2D>("bg_interface");
			
            for (int i = 0; i < joueur.life; i++)
            {
                vie.Push(new Rectangle(Game1.bg_width + marge, (int)(Game1.Resolution.Y * 0.33), this.sprite.Bounds.Width, this.sprite.Bounds.Height));
                marge += 25;
            }
        }
        //Méthodes
        //Draw
        public void Draw(SpriteBatch spritebatch)
        {
            spritebatch.Draw(fond, new Rectangle(Game1.bg_width, 0, fond.Bounds.Width, fond.Bounds.Height), Color.White);

            foreach (Rectangle poscoeur in vie)
            {
                spritebatch.Draw(sprite, poscoeur, Color.White);
            }
        }

    }
}
